## hay que hacer las pruebas unitarias

class Fraccion:
    def __init__(self, n=0, d=1):
        # tenemos que poner lo que tiene una fraccion: 1 num y 1 denom
        # no podemos poner 2 num y 2 denom
        # podemos poner o no los valores por defecto
        self.numerador = n
        self.denominador = d
    
    #si es una funcion: el nombre sería sumar
    #si es un metodo: es una orden asique suma
    def suma (self, otro):
        #coge cosas y me altera algo
        self.numerador = self.numerador*otro.denominador + self.numerador*otro.denominador+ otro.numerador*self.denominador, self.denominador*otro.denominador
        self.denominador = self.denominador*otro.denominador
        return self


    def sumar(izq,dcha):
        # es una funcion: no tiene self
        #coge cosas y me devuelve algo
        return Fraccion(izq.numerador*dcha.denominador + izq.denominador*dcha.numerador, dcha.denominador*izq.denominador)



### programa principal
unmedio = Fraccion(1,2)
uno = Fraccion(1)


#misuma = uno + unmedio
misuma = uno.suma(unmedio) #aqui el uno cambiaria de valor
# uno seria self y unmedio otro
misuma = Fraccion.sumar(uno,unmedio) #ninguno se ve alterado
# uno sería izq y unmdeio dcha
