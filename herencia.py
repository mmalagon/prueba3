class Animal:
    def __init__(self, p):
        self.peso = p
        
#el mamifero es como si fuera un animal, pero tiene algo especial
#forma de modelar algo a partir de otro modelo
#esto es parecido a esto, lo que pasa es que... = herencia
class Mamifero(Animal):
    def __init__(self, p, c):
        super().__init__(p) #padre construyete y toma el peso
        #tiene otras cosas nuevas
        #ahora me construyo yo: necesito crias
        self.crias = c

p1 = Animal (30)
p2 = Mamifero(25, 8) #mamifero ocupa mas memoria que animal
