import unittest

class TestFraccion(unittest.testCase):
    def test_suma01(self):

        f1 = Fraccion(1,2)
        f2 = fraccion(1,2)

        suma = f1.suma(f2)

        self.assertEqual(suma.numerador, 1)
        self.assertEqual(suma.denominador, 1)

    def test_suma02(self):

        f1 = Fraccion(1,2)
        f2 = Fraccion(-1,2)

        suma = f1.suma(f2)

        self.assertEqual(suma.numerador, 0)
        self.assertEqual(suma.denominador, 1)

if __name__ == '__main__':
    unittest.main()