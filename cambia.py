#BIEN
#estamos jugando con las cabras
def cambia (a, b):
    tmp = a.propietario
    a.propietario = b.propietario
    b.propietario = tmp

    #a.propietario, b.propietario = b.propietario, a.propietario #solo cambia lo que hay en propietario, no devuelve nada

#MAL
#estamos jugando con las cuerdas
def cambia2 (a, b):
    tmp = a
    a = b
    b = tmp

class Vehiculo:
    def __init__(self, p):
        self.propietario = p


#cabra=instancia (objeto)
#referencia=cuerda
#clases-referencias
#datos primitivos-datos primitivos (valores que no llaman a la clase)

p1 = Vehiculo("pepe") #hacemos una cabra(propietario = pepe) y la guardamos en p1. Me dan algo para que lo agarre
p2 = Vehiculo("ana")

#p3 = p1
#p4 = p2
#p1 = p2
#p1.propietario="Luis" #cuerda de la cabra de p1 y cambia el propietario
#este tambien es juanita porq la flecha de p1 ahora indica a donde va p2 (lo hemos cambiado antes)
#p2.propietario="Juanita"

cambia(p1, p2)
print ("p1 =", p1.propietario)
print ("p2 =", p2.propietario)

#hay que entender muy bien lo de las cuerdas y las cabras
#casi siempre cae alguna pregunta en el examen