class Vehiculo:
    def __init__(self, p):
        self.propietario = p
        
class Coche(Vehiculo):
    def __init__(self,p, r):
        super().__init__(p) 
        self.ruedas = r

p1 = Vehiculo("sara")
p2 = Coche("paola", 4)